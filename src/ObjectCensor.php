<?php

namespace Censor;


include 'Utilities/Text.php';

use Censor\Utilities\Text;


class ObjectCensor extends AbstractCensor
{

    /**
     * ObjectCensor constructor.
     */
    public function __construct()
    {
    }


    /**
     * @param array $censoredWords. Listado de palabras a reemplazar
     * @param string $text. Texto dado.
     * @return string. Texto reemplazado
     */
    public function __invoke(array $censoredWords, string $text) : string
    {
        $Text = new Text();

        array_walk($censoredWords, [ $Text, 'replaceText' ], $text);

        $replacedText = $Text->getReplacedText();

        return $replacedText;
    }
}
