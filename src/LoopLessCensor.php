<?php

namespace Censor;

class LoopLessCensor extends AbstractCensor
{

    /**
     * @var string Texto reemplazado
     */
    private $replacedText = '';


    /**
     * LoopLessCensor constructor.
     */
    public function __construct()
    {
    }


    /**
     * @param array $censoredWords. Lista constante de palabras a reemplazar en un texto dado.
     * @param string $text. Texto a reemplazar
     * @return string $replacedText. Texto reemplazado
     */
    public function __invoke(array $censoredWords, string $text) : string
    {
        array_walk($censoredWords,  array($this, 'getReplacedText'), $text);

        return $this->replacedText;
    }


    /**
     * Obtenemos el texto reemplazado
     * @param string $word. Palabra a reemplazar
     * @param int $aWordsIndex. index de la palabra dentro del array
     * @param string $text. Texto dado para reemplazar
     */
    private function getReplacedText($word, $aWordsIndex, $text)
    {
        $wordLength = strlen($word);

        $replace = str_pad('',  $wordLength, "*");

        if ($aWordsIndex == 0)
        {
            $this->replacedText = str_ireplace($word, $replace, $text);
        }
        else
        {
            $this->replacedText = str_ireplace($word, $replace, $this->replacedText);
        }
    }

}
