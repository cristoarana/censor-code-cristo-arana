<?php
/**
 * Created by PhpStorm.
 * User: cristo
 * Date: 9/11/18
 * Time: 19:49
 */

namespace Censor\Utilities;


class Character
{

    /**
     * @var string $characters
     */
    private $characters;

    /**
     * @var string $replacement
     */
    private $replacement;



    /**
     * Character constructor.
     * @param $characters. Palabra dada para realizar diferentes operaciones con ella
     */
    public function __construct($characters)
    {
        $this->characters = $characters;
        $this->replacement = "*";
    }


    /**
     * Devuelve una cadena con los caracteres reemplazados
     * @return string caracteres reemplazados
     */
    public function getReplacedCharacters()
    {
        return str_pad('',  $this->characters, $this->replacement);
    }
}