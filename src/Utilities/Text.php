<?php
/**
 * Created by PhpStorm.
 * User: cristo
 * Date: 9/11/18
 * Time: 15:14
 */

namespace Censor\Utilities;

include 'Words.php';



class Text
{

    /**
     * @var string Texto reemplazado
     */
    public $replacedText;



    /**
     * Text constructor.
     */
    public function __construct()
    {
        $this->replacedText = '';
    }


    /**
     * Obtenemos el texto reemplazado
     * @param string $word. Palabra a reemplazar
     * @param int $aWordsIndex. index de la palabra dentro del array
     * @param string $text. Texto dado para reemplazar
     */
    public function replaceText($word, $aWordsIndex, $text)
    {
        $Words = new Words($word);

        $wordLength = $Words->getLengthWord();

        $replace = $Words->getReplacedWord($wordLength);

        $toReplaceText = ($aWordsIndex == 0) ? $text : $this->replacedText;

        $this->replacedText = $Words->replaceWordsInText($word, $replace, $toReplaceText);

    }

    /**
     * Obtiene el texto reemplazado
     * @return string Texto reemplazado
     */
    public function getReplacedText()
    {
        return $this->replacedText;
    }
}