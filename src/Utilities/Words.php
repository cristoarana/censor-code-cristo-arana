<?php
/**
 * Created by PhpStorm.
 * User: cristo
 * Date: 9/11/18
 * Time: 15:12
 */

namespace Censor\Utilities;

include 'Character.php';



class Words
{

    /**
     * @var $word
     */
    private $word;



    /**
     * Words constructor.
     * @param $word. Palabra dada para realizar diferentes operaciones con ella
     */
    public function __construct($word)
    {
        $this->word = $word;
    }


    /**
     * Obtenemos la longitud de una palabra
     * @return int. Longitud de palabra
     */
    public function getLengthWord()
    {
        return strlen($this->word);
    }


    /**
     * Obtenemos la palabra reemplazada por asteríscos
     * @param int $wordLength. Longitud de una palabra
     * @return string. Palabra reemplazada por asteríscos
     */
    public function getReplacedWord($wordLength)
    {
        $Characters = new Character($wordLength);

        return $Characters->getReplacedCharacters();
    }


    /**
     * Reemplaza palabras dentro de un texto
     * @param $word. Palabra a reemplazar
     * @param $replace. Cadena de caracteres de reemplazo
     * @param $text. Texto donde reemplazar
     * @return mixed
     */
    public function replaceWordsInText($word, $replace, $text)
    {
        return str_ireplace($word, $replace, $text);
    }
}