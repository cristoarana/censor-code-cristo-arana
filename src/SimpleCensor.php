<?php

namespace Censor;



class SimpleCensor extends AbstractCensor
{

    /**
     * SimpleCensor constructor.
     */
    public function __construct()
    {
    }


    /**
     * @param array $censoredWords. Listado de palabras a reemplazar
     * @param string $text. Texto dado.
     * @return string. Texto reemplazado
     */
    public function __invoke(array $censoredWords, string $text) : string
    {
        foreach ($censoredWords as $censoredWord)
        {
            $censoredWordLength = strlen($censoredWord);

            $replace = '';

            for ($x = 0; $x < $censoredWordLength; $x++)
            {
                $replace .= '*';
            }

            $text = str_ireplace($censoredWord, $replace, $text);
        }

        return $text;
    }

}